<?php

declare(strict_types=1);

class_alias(\Kubomikita\Commerce\Routing\Cli\BaseRouter::class, \Kubomikita\Commerce\Routing\CliRouter::class);
class_alias(\Kubomikita\Commerce\Routing\Ajax\Router::class, \Kubomikita\Commerce\Routing\AjaxRouter::class);
class_alias(\Kubomikita\Cli\ICli::class, \Kubomikita\Commerce\Routing\Cli\ICli::class);
class_alias(\Kubomikita\Commerce\Routing\Ajax\Router::class, \Kubomikita\Ajax\Router::class);
class_alias(\Kubomikita\Commerce\Routing\Router::class,\LinkMapper::class);
class_alias(\Kubomikita\Commerce\Routing\Router::class,\Kubomikita\Commerce\AppRouter::class);
class_alias(\Kubomikita\Commerce\Routing\RouterInterface::class, \Kubomikita\IRouter::class);