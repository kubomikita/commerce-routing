<?php

namespace Kubomikita\Commerce\Routing;

use Kubomikita\Commerce\ConfiguratorInterface;
use Nette\DI\Container;

interface RouterInterface {
	//public function getContainer() : Container;
	public static function create(Container $container);
	public static function process(ConfiguratorInterface $configurator) : bool;
}