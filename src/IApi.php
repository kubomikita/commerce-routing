<?php

namespace Kubomikita\Commerce\Routing\Api;

use Nette\Http\IRequest;

interface IApi {
	public function setArgs(array $args = []);
	public function logRequest(IRequest $request, ?\Throwable $e = null) : void;
	public function authenticate();
	public function beforeStartup();
}