<?php

namespace Kubomikita\Commerce\Routing;


use Nette\Http\Request;
use Tracy\IBarPanel;

class RoutingPanel  implements IBarPanel {
	/** @var Request  */
	protected $request;

	protected $url;
	protected $post;
	public function __construct(Request $request) {
		$this->request = $request;
		$url = $this->request->getUrl();
		$query = $url->getQueryParameters();
		$this->post = $this->request->getPost();
		foreach ($this->post as $k=>$v){
			unset($query[$k]);
		}
		unset($query["seolink"]);
		unset($query["api"]);
		unset($query["query"]);
		$this->url = ($url->withQuery($query)->withScheme("")->withHost("")->getAbsoluteUrl());
	}

	public function getPanel() {

		ob_start(function () {});
		$request = $this->request;
		require __DIR__ . '/templates/RoutingPanel.panel.phtml';
		return ob_get_clean();
	}

	public function getTab() {
		//$this->analyse($this->router);
		ob_start(function () {});

		$url = $this->url;
		$method = $this->request->getMethod();

		require __DIR__ . '/templates/RoutingPanel.tab.phtml';
		return ob_get_clean();
	}
}