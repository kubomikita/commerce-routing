<?php
namespace Kubomikita\Commerce\Routing\Ajax;

use Commerce\Manager\Manager;
use Kubomikita\Commerce\AppRouter;
use Kubomikita\Commerce\Configurator;
use Kubomikita\Commerce\ConfiguratorInterface;
use Commerce\ControllerFactory;
use Kubomikita\Commerce\DatabaseService;
use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\Commerce\Routing\RouterInterface;
use Kubomikita\Form\Response\Alert;
use Latte\Engine;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Url;
use Nette\Security\User;
use Nette\Utils\Callback;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Tracy\Debugger;

class Router implements RouterInterface {
	/** @var \Nette\Http\Request */
	public $httpRequest;
	/** @var ConfiguratorInterface */
	public $context;

	public static function create( Container $container ) {
		$conf = $container->getByType(ConfiguratorInterface::class);
		$ajax = new self($conf);
		exit;
	}

	public  static function process(ConfiguratorInterface $configurator): bool
	{
		$request = $configurator->getByType(Request::class);
		if(!$configurator->isCli()) {
			if ($request->isAjax()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Router constructor.
	 *
	 * @param $configurator \Kubomikita\Commerce\ConfiguratorInterface
	 */
	public function __construct(Configurator  $configurator) {

		\LangStr::$locale = $configurator->getLanguage();

		$this->context = $configurator;
		$this->httpRequest = $configurator->getService("request");
		
		$response = null;

		try {
			ob_start();
			if ( $this->httpRequest->getQuery( "mod" ) ) {
				$this->old();
				/** @var Connection $db */
				//$db = $this->context->getByType( Connection::class );
				//DatabaseService::disconnect( $db );
				//exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "mod." ) !== false ) {
				/** @var User $User */
				$User = $configurator->getService( "user" );
				$User->getStorage()->setNamespace( "admin" );
				if(function_exists("init_auth_ajax")) {
					init_auth_ajax($User);
				} else {
					$manager = $configurator->getByType(Manager::class);
					$manager->run();
				}
				$qry = $this->httpRequest->getUrl()->getQueryParameters();
				$qry += $this->httpRequest->getPost();

				call_user_func( "action_" . $qry["action"], $qry );
				/** @var Connection $db */
				$db = $this->context->getByType( Connection::class );
				//DatabaseService::disconnect( $db );
				exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "rpc.php" ) !== false ) {
				//bdump($this);
				//exit;
				$this->oldAdminAjaxHandler();
				/** @var Connection $db */
				$db = $this->context->getByType( Connection::class );
				//DatabaseService::disconnect( $db );
				exit;
			} elseif ( strpos( $this->httpRequest->getUrl()->getPath(), "ajaxHandler.php" ) !== false ) {
				$this->oldAjaxHandler();

				//exit;
			} elseif ( $this->httpRequest->getQuery( "render" ) ) {
				$model  = $this->httpRequest->getQuery( "controller" );
				$action = $this->httpRequest->getQuery( "action" );
				$params = $this->httpRequest->getQuery( "params" );
				//$n = new $model;
				$n = $configurator->getService( "controller" )->createOne( $model );
				if(!method_exists($n, $action)) {
					//throw
				}
				echo $n->{$action}($this, $params);
			} elseif (($method = $this->httpRequest->getQuery("do")) && ($presenter = $this->httpRequest->getQuery("presenter"))) {
				$controller = $this->context->getByType(\Commerce\ControllerFactory::class)->createOne($presenter);
				echo $controller->objRender(null, ["view" => $method . ".latte"]);
			} elseif(($method = $this->httpRequest->getQuery("doValidate")) && ($presenter = $this->httpRequest->getQuery("presenter"))) {
				$controller = $this->context->getByType(\Commerce\ControllerFactory::class)->createOne($presenter);
				echo $controller->objRender(null, ["view" => $method . ".latte"]);
			} elseif (($method = $this->httpRequest->getQuery("renderBlock")) && ($presenter = $this->httpRequest->getQuery("presenter"))){
				$controller = $this->context->getByType( \Commerce\ControllerFactory::class )->createOne( $presenter );
				echo $controller->render(null, ["view" => $method.".latte"]);
			} else {
				$model  = $this->httpRequest->getQuery( "controller" );
				$action = $this->httpRequest->getQuery( "action" );

				if ( $model == null or $action == null ) {
					//echo 'Bad request';
					bdump($this->httpRequest);
					throw new AbortException( "Bad ajax request" );
					exit;
				}

				$controller = $this->context->getService( "controller" )->createOne( $model );
	

				if ( ! method_exists( $controller, $action ) ) {
					throw new AbortException( "Bad ajax request. Controller or Action not defined. (not exists)" );
					exit;
				}
				/** @var User $User */
				//$User = $configurator->getService("user");
				//$User->getStorage()->setNamespace("admin");
				//init_auth($User);

				$approuter = new \Kubomikita\Commerce\Routing\Router();
				$seolink   = $this->httpRequest->getQuery( "seolink" );
				if ($seolink !== null && strlen( $seolink ) > 0 ) {
					$approuter = AppRouter::findByLink( $seolink );
				}
				if ( Callback::isStatic( [ $controller, $action ] ) ) {
					$static = $model . "::" . $action;
					if ( version_compare( PHP_VERSION, "7.0.0" ) >= 0 ) {
						echo $static( $this, $approuter );
					} else {
						echo call_user_func( $static, $this, $approuter );
					}
				} else {
					//echo "calling non static method";
					$callback = [ $controller, $action ];
					echo $callback($this, $approuter);
//					echo Callback::invoke( [ $controller, $action ], $this, $approuter );
				}
			}

			$response = ob_get_clean();
		} catch (BadRequestException $e){
			$r = new Alert();
			$r->setDanger();
			$r->setReload(true, 3000);
			$array      = explode("\\", get_class($e));
			$r->title   = $e->getCode() . " " . end($array);
			$r->message = $e->getMessage();
			echo $r->render();
		} catch (\Exception $e){
			echo '<div class="alert alert-danger mb-0"> <i class="fas fa-exclamation-triangle mr-2"></i> '.$e->getMessage().'</div>';
			exit;
		}

		if($response !== null){
			try {
				$json = Json::decode($response);
				echo $response;
			} catch (JsonException $e){
				echo $response;
				/** @var \Kubomikita\Commerce\DataLayer\Container $dataLayerContainer */
				$dataLayerContainer = $this->context->getByType(\Kubomikita\Commerce\DataLayer\Container::class);
				bdump($dataLayerContainer->getDataLayer(),"AJAX DataLayer");
				echo $dataLayerContainer->setType(\Kubomikita\Commerce\DataLayer\Container::CONTAINER_AJAX)->getContainer();
			}
		}

		/** @var Connection $db */
		//$db = $this->context->getByType( Connection::class );
		//DatabaseService::disconnect( $db );

	}

	public function getQuery($key = null){
		if($key === null) {
			return $this->httpRequest->getQuery();
		}
		return $this->httpRequest->getQuery($key);
	}

	public function oldAdminAjaxHandler(){
		/*if(defined("APP_DIR")) {
			include APP_DIR . "manager/lib.menu.php";
		} else {
			include __DIR__ . "/../../../../manager/lib.menu.php";
		}*/
		\AJAX::register('menu_tab','\Menu','setActiveTab');
		\AJAX::register('lang','\Menu','setLang');
		\AJAX::call($_REQUEST['m']);
	}
	public function oldAjaxHandler(){
		$condition = true;
		
		if($condition){
			if($_GET["function"]){
				if($_GET["type"]=="content"){
					$func=$_GET["function"];
					if(function_exists($func)){
						call_user_func($func,$_GET);
					} else {
						echo '<div class="alert alert-danger"><b>AjaxContent:</b> Funkcia '.$func.' neexistuje.</div>';
					}

				} else {
					$func=$_GET["function"];
					$method="_".strtoupper($_GET["method"]);
					$m=$_GET["method"];
					if($m == "POST"){
						$data = $_POST;
					} elseif($m == "GET"){
						$data = $_GET;
					}

					$ajax=$data["ajax"];
					$spam=$data["my_email"];

					if(strtolower($_SERVER["REQUEST_METHOD"]) == strtolower($m)){

						if($ajax and $spam == "nospam"){
							unset($_GET["function"]);
							unset($_GET["method"]);
							unset($data["ajax"]);
							unset($data["my_email"]);
							$req=array();
							$valid=true;
							if($_GET["ajax_load"]){
								$value = $data["value"];
								$ses=$_SESSION["FormRequired"][$data["formId"]][$data["input_name"]];
								if(!empty($ses)){
									if(trim($value) == ""){
										$req[]=$ses;
										$req_js[] = "#".$data["formId"]." input[name=\'".$data["input_name"]."\']";
										$valid=false;
									}

								}
								$ses=$_SESSION["FormPattern"][$data["formId"]][$data["input_name"]];
								if(!empty($ses)){
									$ch = \FormItem::checkPattern($ses["pattern"],$value);
									if($ch !== null){
										if($ch == false){
											if(trim($value) != ""){
												$req[]=$ses["patternText"];
												$req_js[] = "#".$data["formId"]." input[name=\'".$data["input_name"]."\']";
											}
											$valid=false;
										}
									}
								}
							} else {
								$EQUALTO = $_SESSION["FormEqualTo"][$data["formId"]];
								if(!empty($EQUALTO)){
									foreach ($EQUALTO as $key => $v) {
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$key];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										if($data[$key] !== $data[$v["field"]]){
											$req[]=$v["text"];
											$req_js[] = "#".$data["formId"]." input[name=\'$key\']";
											$valid=false;
										}
									}
								}
								if(!empty($_SESSION["FormRequired"][$data["formId"]])){
									foreach($_SESSION["FormRequired"][$data["formId"]] as $name => $val){
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$name];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										if(trim($data[$name]) == ""){
											$req[]=$val;
											$req_js[] = "#".$data["formId"]." input[name=\'$name\'], #".$data["formId"]." textarea[name=\'$name\'], #".$data["formId"]." select[name=\'$name\']";
											$valid = false;
										}
									}

								}
								if(!empty($_SESSION["FormPattern"][$data["formId"]])){
									foreach($_SESSION["FormPattern"][$data["formId"]] as $name => $val){
										$ddd = $_SESSION["FormDependsOn"][$data["formId"]][$name];
										$dddd = (bool)$data[$ddd];
										if($ddd != null and !$dddd) { continue;}
										$ch = \FormItem::checkPattern($val["pattern"],trim($data[$name]));
										if($ch !== null){
											if($ch == false){
												if(trim($data[$name]) != ""){
													$req[]=$val["patternText"];
													$req_js[] = "#".$data["formId"]." input[name=\'$name\'],#".$data["formId"]." select[name=\'$name\'],#".$data["formId"]." textarea[name=\'$name\']";
												}
												$valid=false;
											}
										}
									}
								}
							}

							if(!$valid){
								echo '<div class="alert alert-danger">';
								foreach ($req as $r) {
									echo "<i class=\"fa fa-times\"></i> ".$r . "<br>";
								}
								echo '</div>';
								echo '<script>';
								foreach($req_js as $rr){
									echo "setInputClass('$rr','has-error');";
								}
								echo '</script>';
							}

							if(my_function_exists($func)){
								$dt = $data;
								if(!empty($_FILES)){
									$dt = array();
									$dt = array_merge($data,$_FILES);
								}

								call_user_func($func,$dt,$ajax,$m,$valid);
							}
						}
					} else {
						echo "<b>Error:</b> metoda odoslania nesuhlasi";
					}
				}
			}
		} else {
			header('Content-type: text/html; charset=UTF-8');
			//echo '<div style="color:red;margin-bottom:20px;">Pravdepodobne používate <strong>zastaralý prehliadač</strong>, to je príčinou nefunkčnosti našej stránky. <strong>Odporúčame Vám</strong> nainštalovať si <strong>novšiu verziu prehliadača</strong>, alebo skúste si stiahnuť a nainštalovať <a href="http://www.google.com/chrome/">Google Chrome</a> prehliadač. Môžete taktiež <strong>využiť, telefonickú objednávku (+421 914 777 222) alebo objednávku <a href="mailto:shop@protein.sk">e-mailom</a></strong>.</div>';
			//echo "<div style=\"color:red\">Ak sa Vam tato chyba zobrazila po odoslani formulara, je mozne ze Vas prehliadac ma nacitanu staru verziu stranky. Prosim obnovte stranku pomocou kombinacie tlacitiel <b>CTRL + F5</b>. Ak sa Vam tato chyba objavy znova, skuste to este raz alebo nas kontaktuje na <b>+421 914 777 222</b> alebo e-mailom <a href=\"mailto:shop@protein.sk\"><b>shop@protein.sk</b></a></div>";
		}
	}
	/**
	 * Back compatibility - Render ajax request
	 */
	public function old() {

		/** @var \Commerce\ControllerFactory $\Commerce\ControllerFactory */
		$controllerService = $this->context->getByType(\Commerce\ControllerFactory::class);

		if($this->httpRequest->getQuery("mod") == "kosik_hover"){
			echo "@deprecated";
		}
		if($this->httpRequest->getQuery("mod") == "subcategories"){
			//include "commerce/view/CategoryList/Subcategory.phtml";
			echo "@deprecated";
		}
		if($this->httpRequest->getQuery("mod") == "miniCart"){
			\Cart_controller::handleMiniCart();
		}
		if($this->httpRequest->getQuery("mod")=='kosik'){
			\Cart_controller::handleHeaderCart($this);
		};
		if($this->httpRequest->getQuery("mod")=='kosik_order'){
			\Cart_controller::handleCart($this);
		};
		if($this->httpRequest->getQuery("mod")=='kosik_order_step1'){
			\Cart_controller::handleOrderStep1();
		};
		if($this->httpRequest->getQuery("mod")=='kosik_added' && $this->httpRequest->getQuery("tovar")){
			\Cart_controller::handleAdded($this->httpRequest);
		};
		if($this->httpRequest->getQuery("mod")=="vyrobca_load"){

			$Vyrobcovia_new = \TovarVyrobca::fetch($this->httpRequest->getQuery("type"));
			$expect = strtoupper(substr($this->httpRequest->getQuery("type"),0,1));
			echo '<ul id="vyrobcovia" >';
			foreach($Vyrobcovia_new as $V){
				//dump($V->image());
				if($V->count_items() > 0){
					echo '<li><a href="'.eshop_vyrobca_seo_link_page($V,1).'">'.$V->nazov.' '.$V->tag_show($expect).'</a></li>';
				}
			}
			echo '</ul>';
		}

		if($this->httpRequest->getQuery("mod")=="control" and $this->httpRequest->getQuery("controller")){
			$data = (array) json_decode($this->httpRequest->getQuery("data"));
			//dump($this,$data,$this->httpRequest->getQuery("controller"));
			return call_user_func($this->httpRequest->getQuery("controller"),$data);
		}
		if($this->httpRequest->getQuery("mod") == "sameAs" and $this->httpRequest->getQuery("tovar")){
			$sameController = $controllerService->createOne(\SimilarProducts_controller::class);
			//$sameController->actionAlsoBought();
			$sameController->setView("AlsoBought");
			echo $sameController->render();
		}

		if($this->httpRequest->getQuery("mod")=='tovarhodnotenie'){
			$Tovar = new \Tovar($this->httpRequest->getQuery("tovar"));
			$TH = new \TovarHodnotenie($Tovar->id);
			$TH->tovar_object = $Tovar;
			$TH_CURRPAGE = (int)$this->httpRequest->getQuery("page");
			$TH_LIST = $TH->fetchApproved($TH_CURRPAGE);
			$TH_PAGES = ceil($TH->pocet()/$TH->pagesize);
			include('commerce/view/ProductDetail/Rating.phtml');
		};

		if($this->httpRequest->getQuery("mod")=='tovarotazka'){
			$Tovar = new \Tovar($this->httpRequest->getQuery("tovar"));
			$TO_COUNT = \TovarOtazka::pocet($Tovar->id);
			$TO_CURRPAGE = (int)$this->httpRequest->getQuery("page");
			$TO_LIST = \TovarOtazka::fetchTovar($Tovar,$TO_CURRPAGE);
			$TO_PAGES = ceil($TO_COUNT/\TovarOtazka::$pagesize);
			include('commerce/view/ProductDetail/Answer.phtml');
		};
		/**
		USER functions

		 */
		if($this->httpRequest->getQuery("mod")=="predajna_zasoba" && $this->httpRequest->getQuery("tovar_id")){
			\Predajne_controller::handleTovarPredajne($this);
		}
		if($this->httpRequest->getQuery("mod")=='favorites_added'){
			include('commerce/view/ProductDetail/AddedToFavorites.php');
		};
		if($this->httpRequest->getQuery("mod")=='user_fa_add'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserInvoiceAddress.latte");
			$rendered = $controller->render(null, []);
			echo $rendered;
		};
		if($this->httpRequest->getQuery("mod")=='user_da_add'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserDeliveryAddress.latte");
			$rendered = $controller->objRender(null, []);
			echo $rendered;
		};
		if($this->httpRequest->getQuery("mod")=='user_ad_delete'){
			$controller = new \LoginForm_controller();
			$controller->setLatteTemplate("UserDeleteAddress.latte");
			echo $controller->objRender(null,[]);
		};
		if($this->httpRequest->getQuery("mod")=='my_account'){
			include('commerce/view/LoginForm/MyAccount.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='user_details'){
			include('commerce/view/LoginForm/UserDetails.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='detail_objednavky'){
			include('commerce/view/Cart/OrderDetail.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='delete_dialog'){
			bdump($this);
			include('commerce/view/HomePage/DeleteDialog.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='watchdog'){
			$watch = $this->httpRequest->getQuery("watch");//$_GET["watch"];
			$tovar = $this->httpRequest->getQuery("tovar");//$_GET["tovar"];
			//include('commerce/view/ProductDetail/Watchdog.phtml');
			\ProductDetail_controller::handleWatchdog($this);
		};
		if($this->httpRequest->getQuery("mod")=='addRating'){
			include('commerce/view/ProductDetail/AddRating.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='addQuestion'){
			include('commerce/view/ProductDetail/AddQuestion.phtml');
		};
		if($this->httpRequest->getQuery("mod")=='instantsearch'){
			$searchController = $controllerService->createOne(\SearchForm_controller::class);
			$searchController->setView("instantsearch");
			echo $searchController->render();
		}

	}
}