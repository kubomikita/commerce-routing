<?php

namespace Kubomikita\Cli;

interface ICli {

	/*public function log($message/*, $color= "default");*/
	/**
	 * @param array $args
	 *
	 * @return void
	 */
	public function run(array $args = []);
}