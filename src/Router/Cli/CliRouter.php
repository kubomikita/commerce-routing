<?php
declare(strict_types=1);
namespace Kubomikita\Commerce\Routing\Cli;

use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\Routing\RouterInterface;
use Nette\DI\Container;

final class CliRouter implements RouterInterface {

	const COLORS = [
		"default" => "[39m",
		"lightred" => "[91m",
		"lightgreen" => "[92m",
		"magenta" => "[35m",
	];

	public function __construct()
	{
	}

	public static function create( Container $container ) {
		$cli = new self( $_SERVER['argv'], $container);
		exit;
	}

	public static function process( ConfiguratorInterface $configurator ):bool
	{
		if ( $configurator->isCli() ) {
			return true;
		}
		return false;
	}
}