<?php

declare(strict_types=1);

namespace Kubomikita\Commerce\Routing\Cli;

interface CliJobInterface extends \Kubomikita\Cli\ICli {

	public function log(string $message, string $color= "default");
	public function getLog() : string;
}