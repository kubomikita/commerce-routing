<?php

declare(strict_types=1);

namespace Kubomikita\Commerce\Routing\Cli;


use Kubomikita\Commerce\Routing\RouterInterface;
use Nette\DI\Container;

abstract class CliJob implements CliJobInterface {

	protected string $log = '';
	protected Container $container;

	public function __construct(RouterInterface|CliRouter $router)
	{
		$this->container = $router->context;
	}

	/**
	 * @param string
	 * @return void
	 */
	public function log(string $message, string $color = 'default')
	{
		$colors = CliRouter::COLORS;

		$e = explode("\\", get_class($this));
		$shortName = end($e);

		$lineCmd = date("d.m.Y H:i:s") . "\t" . $shortName . "\t" . "\033".($colors[$color] ?? $colors["default"]). $message . "\033".$colors["default"] ."\n";
		$lineLog = date("d.m.Y H:i:s") . "\t" . $shortName . "\t" .  $message . "\n";
		$this->log .= $lineLog;
		echo $lineCmd;
	}

	public function getLog() : string
	{
		return $this->log;
	}

	protected function getXmlFromFtp(){
		$ftp_cred = \Registry::get("container")->getParameter("part");
		$g = false;
		try {
			$ftp = new \Ftp();
			$ftp->connect( $ftp_cred["ftp_server"] );
			$ftp->login( $ftp_cred["ftp_user"], $ftp_cred["ftp_pass"] );
			$ftp->pasv(true);
			$g = $ftp->get( $this->xml_path . $this->xml_filename . ".download", $this->xml_filename, \Ftp::BINARY );
			$ftp->delete( $this->xml_filename );

		} catch (\FtpException $e){
			$this->log($e->getMessage());
		}
		if(!$g){
			$this->log("nepodailo sa stiahnut XML z FTP");
			return false;
		}

		$new_content = file_get_contents($this->xml_path.$this->xml_filename.".download");

		if(file_exists($this->xml_path.$this->xml_filename)){
			$old_content = file_get_contents($this->xml_path.$this->xml_filename);
			$this->log("md5 checksum: NEW ".md5($new_content));
			$this->log("md5 checksum: OLD ".md5($old_content));
			if(md5($new_content) == md5($old_content)){
				unlink($this->xml_path.$this->xml_filename.".download");
				$this->log("XML sa nezmenilo");
				return false;
			} else {
				file_put_contents($this->xml_path.$this->xml_filename,$new_content);
				unlink($this->xml_path.$this->xml_filename.".download");
				$this->log("Nove XML bolo nahrate");
			}
			//print_r(md5($new_content),md5($old_content));
		} else {
			file_put_contents($this->xml_path.$this->xml_filename,$new_content);
			unlink($this->xml_path.$this->xml_filename.".download");
		}

		if(!file_exists($this->xml_path.$this->xml_filename)){
			$this->log("XML neexistuje");
			return false;
		}
		return true;
	}

	abstract public function run(array $args = []);

}