<?php
namespace Kubomikita\Commerce\Routing;
use Nette\DI\Container;

class DefaultRedirectsRouter  implements RouterInterface  {
	public static function create( Container $container ) {
		return new static();
	}
}