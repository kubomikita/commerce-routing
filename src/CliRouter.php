<?php
namespace Kubomikita\Commerce\Routing\Cli;

use Cron\CronExpression,
	Nette\Utils\Strings,
	Nette\Utils\Json,
	Nette\Utils\Finder,
	Tracy\Debugger;
use Kubomikita\Commerce\ApplicationInterface;
use Kubomikita\Commerce\Configurator;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\DatabaseService;
use Kubomikita\Commerce\Routing\RouterInterface;
use Kubomikita\Format;
use Kubomikita\IRouter;
use Kubomikita\Service;
use Nette\Caching\Storage;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Http\Session;
use Nette\Http\UrlScript;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Tracy\ILogger;
use Tracy\Logger;

class BaseRouter implements RouterInterface {
	protected static $tableName = "ec_cron";
	public $db;
	/** @var Container */
	public $context;
	protected $jobName = null;
	protected $jobArgs = [];
	protected $cliArgs = [];

	protected string $log = '';

	const CMD_COLORS = [
		"default" => "[39m",
		"lightred" => "[91m",
		"lightgreen" => "[92m",
		"magenta" => "[35m",
	];

	public static function create( Container $container ) {
		$cli = new self( $_SERVER['argv'], $container);
		exit;
	}

	public static function process( ConfiguratorInterface $configurator ):bool
	{
		if ( $configurator->isCli() ) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param string
	 * @return void
	 */
	protected function log($message, $color = 'default')
	{
		$colors = self::CMD_COLORS;

		$e = explode("\\", get_class($this));
		$shortName = end($e);

		$lineCmd = date("d.m.Y H:i:s") . "\t" . $shortName . "\t" . "\033".($colors[$color] ?? $colors["default"]). $message . "\033".$colors["default"] ."\n";
		$lineLog = date("d.m.Y H:i:s") . "\t" . $shortName . "\t" . $message . "\n";
		$this->log .= $lineLog;
		echo $lineCmd;
	}

	public function __construct($args = null, ?Container $container = null) {

		if(!$container->getByType(Configurator::class)->isProduction()) {
			Debugger::enable(Debugger::DEVELOPMENT);
			error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
		}

		$this->context = $container;
		
		/** @var Request $requestService */
		$requestService = $this->context->getByType(Request::class);
		/** @var UrlScript $urlScriptService */
		$urlScriptService = new UrlScript($this->context->parameters['shop']['url']);
		$this->context->removeService('http.request');
		$this->context->removeService('http.urlScript');
		$this->context->addService('http.request', $requestService->withUrl($urlScriptService));
		$this->context->addService('http.urlScript', $urlScriptService);


		$this->db = $container->getByType(Connection::class);

		if($args == null){
			$args = $_SERVER["argv"];
		}
		$this->startup();
		$this->jobName = $args[1];
		$this->cliArgs = $args;
		unset($this->cliArgs[0], $this->cliArgs[1]);
		$this->cliArgs = array_values($this->cliArgs);
		if(isset($args[2]) && is_string($args[2])){
			$this->jobArgs = (array) json_decode($args[2]);
		} else {
			$this->jobArgs = $args[2] ?? null;
		}

		if($this->jobName === null) {
			$this->log('Please specify CLI action to execute', "lightred");
			exit;
		}

		$functionName = "action".ucfirst($this->jobName);
		try{
			if(!method_exists($this,$functionName)){
				$this->log("Command line action '$functionName' not exists in '".self::class."'.", "lightred");
				exit;
			}
			$this->{$functionName}($this->jobArgs);
		} catch(\Throwable $e){
			$this->log($e->getMessage()." - ".$e->getFile()." - ".$e->getLine()." - ".$e->getTraceAsString());
			//var_dump($e);
		}
	}
	private function checkManualStart($cronid,$args){
		if(isset($args["manual"])){
			$c = CronExpression::factory($args["manual"]);
			$fields = [
				"minute"=>$c->getExpression(0),
				"hour" => $c->getExpression(1),
				"day" => $c->getExpression(2),
				"month" => $c->getExpression(3),
				"weekday" => $c->getExpression(4),
			];
			unset($args["manual"]);
			$fields["args"] = Json::encode($args);
			$this->db->query("UPDATE ".self::$tableName." SET", $fields ,"WHERE id = ?",$cronid);
		}

	}
	private function setLast($cronid,$log=""){
		$this->db->query("UPDATE ".self::$tableName." SET",["last"=>new DateTime(),"log"=>$log],"WHERE id = ?",$cronid);
	}
	private function setLog(int $cronid, string $log = ''){
		$this->db->query("UPDATE ".self::$tableName." SET",["log"=>$log],"WHERE id = ?",$cronid);
	}
	private function actionCronJobs(){
		//try {
		$this->log("============== Starting CRON JOBS ==============", "lightgreen");
			$q = $this->db->query( "SELECT * FROM " . self::$tableName . " WHERE enabled=1 ORDER BY priority" );
			foreach ( $q as $cron ) {
				$expression = implode( ' ', [ $cron->minute, $cron->hour, $cron->day, $cron->month, $cron->weekday ] );
				if ( CronExpression::isValidExpression( $expression ) ) {
					$time = CronExpression::factory( $expression );
					if ( $time->isDue() ) {
						try {
							$name  = $cron->model;
							$class = '\\Kubomikita\\Cli\\Cron\\' . $cron->model;
							if ( ! class_exists( $class ) ) {
								throw new \Exception( "Class '\\Kubomikita\\Cli\\Cron\\" . Strings::firstUpper( $name ) . "' not found." );
							}
							$args = $cron->args ? Json::decode( $cron->args, Json::FORCE_ARRAY ) : [];
							$job  = new $class( $this );
							if ( ! ( $job instanceof ICli ) ) {
								throw new \Exception( "Class '$class' not implements ICli" );
							}

							if(($domainDispatcher = $this->context->getByType(ApplicationInterface::class)->getDomainDispatcher()) !== null) {
								$domainDispatcher->setDomain(null);
								if (isset($args['domain'])) {
									$application = $this->context->getByType(ApplicationInterface::class);
									$domainDispatcher->setDomain($args['domain']);
									$domainDispatcher->dispatch($application, false);

									foreach ($domainDispatcher->getDispatchEvents() as $event => $events) {
										foreach ($events as $event) {
											$event($application, $this->context->getByType(Request::class),
												$this->context->getByType(Response::class));
										}
									}
								}

							}
							\Cache::flushStorage();
							foreach ($this->context->findByType(Storage::class) as $serviceName) {
								\Cache::setStorage($this->context->getService($serviceName));
							}

							$job->run( $args );
							$this->checkManualStart( $cron->id, $args );
							$job->log("CRON memory usage: ".\Format::bytes(memory_get_peak_usage()),"lightred");
							$this->setLast( $cron->id, $job->getLog() );

						} catch ( \Throwable $e ) {
							$this->log('An error occured. Unable to finish jobs.', "lightred");
							//dump($e->getMessage());
							$this->log("Cron model: ". $cron->model);
							$this->log("Logger: ".Debugger::getLogger()::class);
							$this->log("CRON ".$cron->model." memory usage: ".\Format::bytes(memory_get_peak_usage()),"lightred");
							$this->setLog($cron->id, $this->log);
							Debugger::log($e, ILogger::EXCEPTION);
							//trigger_error( $e->getMessage(), E_USER_ERROR );
						}

					}

				} else {
					Debugger::log( "Cron ID '" . $cron->id . "' time expression could not be parsed.",
						Debugger::EXCEPTION );
				}

			}
		/*} catch (\Exception $e){
			echo $e->getMessage();
		}*/
		$this->log("=============== Ending CRON JOBS ===============","lightgreen");
		exit;
	}

	private function actionTestCli(){
		//dump(session_status(), $_SESSION, $this->context->getByType(Session::class));
		//print_r($this->db);
		//print_r($this->context->parameters);
		echo $this->context->parameters['shop']['url']."\n";
		echo (string) $this->context->getByType(Request::class)->getUrl(). "\n";
		echo "Hello world";
	}

	private function actionDeleteOldPrice()
	{
		echo "lol\n";
		try {
			$q = $this->db->query("SELECT * FROM `ec_tovar` WHERE `metadata` LIKE '%s:16:\"konkurencna_cena\";s:3:\"-5%\"%'");
			foreach ($q as $row) {
				$t                   = new \Tovar($row->id);
				$t->konkurencna_cena = '';
				$t->save_meta();
			}
		} catch (\Throwable $e) {
			echo $e->getMessage()."\n";
		}
	}

	private function actionTagAdd() {
		if(empty($this->cliArgs) || count($this->cliArgs) < 2){
			$this->log("please specify tag id and category id");
			exit;
		}
		list($tagId, $categoryId) = $this->cliArgs;

		$kategoria = new \Kategoria($categoryId);
		$kategorie = $kategoria->fetchChildrenNodes();
		array_unshift($kategorie, $kategoria);
		//dump($kategoria->fetchChildrenNodes());
		//exit;

		foreach ($kategorie as $k){
			$i = 0;
			//dump((string) $k->nazov);
			$s = new \TovarSearchResult();
			$s->kategoria = $k->id;
			$s->notagy = [$tagId];
			foreach ($s->result() as $T){
				$T->tag_add($tagId);
				$T->save();
				$i++;
			}
			//if($i > 0) {
				$this->log("Tag '$tagId' pridaný $i tovarom v kategórii {$k->nazov}");
			//}
			//dump($s->count_fast());
		}

		//dump($tagId, $categoryId);
	}

	private function actionImageAdd(){
		$dir = TEMP_DIR."images/";
		/** @var \SplFileInfo $file */
		foreach (Finder::find("*.jpg")->from($dir) as $file){
			$info = pathinfo((string) $file);
			$this->log($file->getFilename() );
			if(($Tovar = \Tovar::get_by_code($info["filename"]))->id !== null ){
				$img = new \TovarImg();
				$img->tovar = $Tovar->id;
				$img->content = file_get_contents((string) $file);
				$img->save();
				$img->saveData();

				$this->log((string) $Tovar->nazov);
				FileSystem::delete((string) $file);
			}
		}
		
	}

	private function actionCmsImgToFileStore(){
		echo 'start'.PHP_EOL;
		$this->db = Service::get("database");
		$q = $this->db->query("SELECT * FROM cms_img_images WHERE img IS NOT NULL ORDER BY id DESC LIMIT 100");
		$path = WWW_DIR."bindata/";
		foreach($q as $r){
			$name = "cms-".$r->id.".jpg";
			echo 'Creating '.$name.PHP_EOL;
			file_put_contents($path.$name,$r->img);
			$this->db->query("UPDATE cms_img_images SET img=? WHERE id = ?",NULL,$r->id);
		}
		echo 'end'.PHP_EOL;
	}
	private function actionCommerceImgToFileStore(){
		echo 'start'.PHP_EOL;
		$this->db = Service::get("database");
		$q = $this->db->query("SELECT * FROM ec_img_images WHERE img IS NOT NULL ORDER BY id DESC LIMIT 100");
		$path = WWW_DIR."bindata/";
		$this->log($path);
		foreach($q as $r){
			$name = "ei-".$r->id.".jpg";
			echo 'Creating '.$name.PHP_EOL;
			file_put_contents($path.$name,$r->img);
			//var_export($r);
			$this->db->query("UPDATE ec_img_images SET img=? WHERE id = ?",NULL,$r->id);
		}
		echo 'end'.PHP_EOL;
	}
	private function actionCategorySvg(){
		$dir = WWW_DIR."assets/img/icons-menu/";
		/** @var \Kategoria $K */
		foreach(\Kategoria::fetchList() as $K){
			$icon = isset($K->metadata["icon"]) ? $K->metadata["icon"] : null;
			//$this->log($K->metadata["icon"]);
			if($icon === null || trim($K->metadata["icon"]) == ""){
				//$this->log($K->seo_name_new);
				if(file_exists($dir.$K->seo_name_new.".svg")) {
					$this->log( $K->nazov . " - " . $K->seo_name_new );
					$K->metadata["icon"] = $K->seo_name_new;
					$K->save();
				}
			}

		}
	}
	private function tovar_get_by_code($kod) {
		$q = $this->db->query("select * from ec_tovar where kod_systemu=?",$kod)->fetch();
		if(!$q){
			return new \Tovar;
		}
		return new \Tovar($q->id);
	}
	public function actionImgByCode(){
		$dir = TEMP_DIR."images_import";
		if(!file_exists($dir)){
			throw new \Exception("Images import dir '$dir' not exists");
		}

		$a = $f = $s = 0;

		foreach(glob($dir."/*.[jJp][pPn][gG]") as $file){
			$e = explode("/",$file);
			$filename = end($e);
			$end = explode(".",$filename);
			$ext = end($end);
			$code = trim(substr($filename,0,stripos($filename,".".$ext)));
			$t = $this->tovar_get_by_code($code);

			if((int) $t->id > 0) {
				$images = count($t->images());

				$this->log( $code . " - " . $t->nazov." - ". $images );
				if($images == 0) {
					$content = file_get_contents( $file );
					if ( trim( $content ) != "" ) {
						/** @var \TovarImg $img */
						foreach ( $t->images() as $img ) {
							$img->delete();
						}
						$I          = new \TovarImg();
						$I->tovarId = $t->id;
						$I->content = $content;
						$I->save();
						$I->saveData();
						//$I->moveTop()
						unlink( $file );
						//$this->log("ukladam novy obrazok");
						$a ++;
					}
					if ( $images < 1 and $t->primarna_kategoria->id != 1000 ) {
						$t->aktivny = 1;
						$t->sync    = 1;
						$t->save();
						$s ++;
					}
				} else {
					unlink( $file );
				}

			} else {
				$f++;
				$this->log("Tovar s kodom '$code' neexistuje.");
			}
		}
		$this->log("--------------------------------------------------");
		$this->log("Priradene: ".$a);
		$this->log("Aktivovane: ".$s);
		$this->log("Kod neexistuje: ".$f);
		$this->log("--------------------------------------------------");
	}
	private function startup(){

	}
}