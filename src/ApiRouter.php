<?php

namespace Kubomikita\Commerce\Routing\Api;


use Kubomikita\Commerce\Application;
use Kubomikita\Commerce\ConfiguratorInterface;

use Kubomikita\Commerce\IDiConfigurator;
use Kubomikita\Commerce\Routing\RouterInterface;

use Nette\Application\ApplicationException;
use Nette\Application\BadRequestException;

use Nette\Application\Responses\JsonResponse;
use Nette\Application\Routers\Route;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\Request;
use Nette\Http\RequestFactory;

use Nette\Http\Response;
use Nette\InvalidArgumentException;
use Nette\Utils\Arrays;

use Nette\Utils\Strings;


class Router implements RouterInterface {
	/** @var IDiConfigurator  */
	private $context;
	/** @var IRequest */
	private $request;
	/** @var IRequest */
	private $globalRequest;
	/** @var string  */
	private $class;
	/** @var string  */
	private $method;
	private $args;
	private $files;
	
	public static function create( Container $container ) {
		$conf = $container->getByType(ConfiguratorInterface::class);
		$request = $container->getByType(Request::class);
		$globalRequest = (new RequestFactory())->createHttpRequest();
		$api = new self( $request, $globalRequest, $conf);
		/** @var Connection $db */
		//$db = $container->getByType(Connection::class);
		//DatabaseService::disconnect($db);
		exit;
	}

	public static function process(ConfiguratorInterface $configurator): bool
	{
		/** @var Request $request */
		$request = $configurator->getByType(Request::class);
		if(!$configurator->isCli()) {
			if ($request->getQuery('api') || Strings::startsWith($request->getUrl()->getPathInfo(), "api/")) {
				return true;
			}
		}
		return false;
	}

	public function __construct(IRequest $request, IRequest $globalRequest, ConfiguratorInterface $context) {
		$this->request = $request;
		$this->globalRequest = $globalRequest;
		$this->context = $context;
		//dumpe();
		if(($query = $this->request->getQuery("query")) === null){
			$query = str_replace('api/', '', $this->request->getUrl()->getPathInfo()) | ($query ?? '');
		}

		//dumpe($this->request->getQuery( "query" ), $query); // //$this->request->getQuery( "query" );
		if((String) $query == ""){
			throw new BadRequestException("Not found.",404);
		}

		@list($class,$method,$args) = explode("/", $query);

		$args = $this->getQueryArguments($args);
		$args += $_REQUEST;
		$this->args = $args;
		$this->files = $_FILES;

		unset($args["api"], $args["query"]);

		if(class_exists("\\Kubomikita\\Commerce\\Api\\" . Route::path2presenter( $class ))) {
			$this->class = "\\Kubomikita\\Commerce\\Api\\" . Route::path2presenter( $class );
		} else {
			$this->class = "\\Kubomikita\\Api\\" . Route::path2presenter( $class );
		}
		try {

			if (!class_exists($this->class)) {
				throw new BadRequestException("Class '" . $this->class . "' not found.");
			}
			if($method === null){
				throw new BadRequestException("Action is not specified");
			}

			$this->method = "action";
			$em = explode("_", $method);
			foreach ($em as $mm) {
				$this->method .= Strings::firstUpper($mm);
			}

			$this->method = Route::path2action($this->method);

			foreach($this->args as $ak=> $ar){
				if(isset($_COOKIE[$ak])){
					unset($this->args[$ak]);
				}
			}


			/** @var BaseApi $job */
			$job = new $this->class($this->context,$this, $this->request);
			if(!($job instanceof IApi)){
				throw new ApplicationException("Class '".$this->class."' must implements IApi");
			}

			if(!method_exists($this->class,$this->method)){
				throw new BadRequestException("Method '".$this->method."' in class '".$this->class."' not found");
			}

			$annotations = \DocBlock::parseComment($job->reflection->getMethod($this->method)); //$job->reflection->getMethod($this->method)->getAnnotations();
			//dumpe($annotations);
			if(isset($annotations["method"]) && !empty($annotations["method"])){
				$methods = Arrays::map($annotations["method"], function ($value){
					return Strings::lower($value);
				});

				if(!in_array(Strings::lower($this->globalRequest->getMethod()),$methods)) {
					throw new ApplicationException( "Only '".implode("|", $annotations["method"])."' request method is allowed, but '".$this->globalRequest->getMethod()."' processed.", 500 );
				}
			}

			$job->setArgs($this->args);
			$job->beforeStartup();
			$job->authenticate();

			$routerArgs = $job->getArgs();
			$actionArgs = [];
			$actionParameters = $job->reflection->getMethod($this->method)->getParameters();
			
			foreach($actionParameters as $param){
				if(isset($routerArgs[$param->name])) {
					if(empty($routerArgs[$param->name]) && $routerArgs[$param->name] !== "0" && !$param->isOptional()){
						throw new BadRequestException("Missing value of argument '\${$param->name}' in method '{$this->class}::{$this->method}()'");
					}
					//dumpe($param->getType()->getTypes()[0]->getName());
					$paramType = $param->getType()->getName(); //Reflection::getParameterType($param);
					//dumpe($paramType, $param->getType()->getName());
					$actionArgs[$param->name] = $routerArgs[$param->name];
					if($paramType !== null){
						settype($actionArgs[$param->name], $paramType);
					}
				} else {
					if(!$param->isOptional()) {
						throw new BadRequestException("Missing argument '\${$param->name}' in method '{$this->class}::{$this->method}()'");
					}
				}
			}

			$job->logRequest($this->globalRequest);
			$job->{$this->method}(... array_values($actionArgs));
			if(method_exists($job, "sendResponse")){
				$job->sendResponse();
			}
		} catch(\Throwable $e){
			//trigger_error($e->getMessage(),E_USER_NOTICE);
			if($this->context->isDebugMode()){
				bdump($e, "Exception");
			}

			$code = $e->getCode() >= 200 ? $e->getCode() : 500;
			$jobExists = isset($job) && $job instanceof IApi;
			$jobThrow = $jobExists && $job->isThrowException();

			if($jobThrow || $code === 500){
				throw $e;
			}
			$response = ["code" => $code, "message" => 'Endpoint not found.'];
			if($this->context->isDebugMode()){
				$response["exception"] =  $e->getMessage();
				$response["exceptionCode"] = $e->getCode();
				$response["file"] = $e->getFile();
				$response["line"] = $e->getLine();
				//$response["trace"] = $e->getTrace()[0];
			}
			$this->sendJsonResponse(['response' => $response], is_string($code) ? 500 : $code);
			/*dumpe($e,$response);


			if(isset($job) && $job instanceof IApi){
				$job->logRequest($this->globalRequest, $e);

				if($job->isThrowException()) {
					throw $e;
				} else {
					$response = ["code" => $code, "message" => 'Endpoint not found.'];
					if($this->context->isDebugMode()){
						$response["exception"] =  $e->getMessage();
					}
					$this->sendJsonResponse(['response' => $response], $code);
				}
			} else {

				if($code === 500) {
					throw $e;
				} else {
					$response = ["code" => $code, "message" => 'Endpoint not found.'];
					if($this->context->isDebugMode()){
						$response["exception"] =  $e->getMessage();
					}
					$this->sendJsonResponse(['response' => $response], $code);
				}
			}*/
		}
	}

	private function getQueryArguments(?string $args = null) : array {
		$args = ($args != null) ? explode(";",$args): [];
		foreach($args as $key=> $a){
			if(strpos($a,":") !== false) {
				@list( $k, $v ) = explode( ":", $a );
				if ( $v !== null ) {
					$args[ $k ] = $v;
					unset( $args[ $key ] );
				}
			}
		}
		return $args;
	}
	public function getMethod() :?string
	{
		return $this->method;
	}
	public function sendJsonResponse(array $body = [], int|string $responseCode = 200) : void
	{
		$this->context->getByType(Application::class)->sendJsonResponse($body, $responseCode);
	}
}