<?php


namespace Kubomikita\Commerce\Routing\Api;


use JetBrains\PhpStorm\NoReturn;
use Kubomikita\Commerce\Application;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\DataLayer\Container;
use Kubomikita\Commerce\Routing\RouterInterface;
use Kubomikita\IRouter;
use Nette\Database\Connection;
use Nette\Database\ResultSet;
use Nette\Database\Row;
use Nette\Http\IRequest;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\InvalidArgumentException;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Nette\Utils\Reflection;
use Nette\Utils\Strings;
use Tracy\Debugger;
use Tracy\Logger;

abstract class BaseApi implements IApi{
	/** @var ConfiguratorInterface */
	protected $context;
	/** @var IRequest */
	protected $request;
	/** @var Response */
	protected $response;
	/** @var IRouter */
	protected $router;
	/** @var Connection */
	public $db;
	/** @var ArrayHash */
	protected $parameters = [];
	/** @var ArrayHash */
	protected $args = [];
	/** @var ArrayHash */
	protected $config = [];
	/** @var bool  */
	protected $logRequests = false;
	protected $logRequestsWithRawBody = false;
	/** @var array  */
	protected $headers = [];
	/** @var string|null  */
	protected $bearerToken = null;
	/** @var bool  */
	protected $throwException = false;

	public $reflection;

	protected array $error = [];

	protected ?\CommerceController $controller = null;
	protected array $snippets = [];
	protected \stdClass $payload;

	/**
	 * BaseApi constructor.
	 *
	 * @param ConfiguratorInterface $context
	 * @param RouterInterface $router
	 * @param IRequest $request
	 */
	#[NoReturn] public function __construct(ConfiguratorInterface $context, RouterInterface $router, IRequest $request) {
		$this->context = $context;
		$this->router = $router;
		$this->request = $request;
		$this->response = $context->getByType(Response::class);
		$this->db = $context->getByType(Connection::class);
		$this->parameters = ArrayHash::from($context->container->parameters);
		$this->headers = $request->getHeaders();
		foreach($this->headers as $k => $v){
			if(Strings::lower($k) === "authorization" && Strings::contains($v, "Bearer")){
				$this->bearerToken = Strings::substring($v, 7);
			}
		}
		$this->reflection = new \ReflectionClass($this); //ClassType::from($this);

		$class = Strings::lower($this->reflection->getShortName());

		//dumpe($class, (new \ReflectionClass($this))->getShortName());
		if(isset($this->parameters[$class])){
			$this->config = $this->parameters[$class];
		}

		$this->payload = new \stdClass();
	}

	/**
	 * @param array $args
	 *
	 * @return $this
	 */
	public function setArgs(array $args = []){
		$escaped = [];
		foreach($args as $k=>$v){
			$escaped[str_replace("amp;","",$k)] = (!is_array($v) ? Strings::substring($this->db->getPdo()->quote($v),1,-1) : $v);
		}
		$this->args = ArrayHash::from($escaped);
		return $this;
	}

	/**
	 * @return ArrayHash
	 */
	public function getArgs() {
		return $this->args;
	}

	/**
	 * @return bool
	 */
	public function isThrowException(): bool {
		return $this->throwException;
	}

	/**
	 * @param IRequest $request
	 * @param \Throwable|null $e
	 */
	public function logRequest( IRequest $request, ?\Throwable $e = null) : void
	{
		if($this->logRequests) {
			$content = $request->getRawBody();
			$content_lenght = strlen($content);
			$content_type = $content_lenght > 0 ? $request->getHeader("content-type") : null;


			$url = $request->getUrl();
			//$url = $url->withQuery()

			$data = [
				"datetime" => new DateTime(),
				"method" => $request->getMethod(),
				"ip" => $request->getRemoteAddress(),
				"url" => str_replace($request->getUrl()->getHostUrl(),"",$request->getUrl()->getAbsoluteUrl()),
				"headers" => json_encode($request->getHeaders()),
				"useragent" => $request->getHeader("user-agent"),
				"content_type" => $content_type,
				"content_lenght" => $content_lenght,
				"exception_message" => ($e instanceof \Throwable) ? $e->getMessage() : null,
				"exception" => ($e instanceof \Throwable) ? json_encode($e) : null,
			];

			if($this->logRequestsWithRawBody){
				$data["content_raw"] = $request->getRawBody();
			}


			$this->db->query("INSERT INTO api_requests",$data);

			if(($insertId = $this->db->getInsertId()) > 0 && $content_lenght > 0) {
				$dir = TEMP_DIR."api/";
				if(!file_exists($dir)){
					FileSystem::createDir($dir);
				}
				FileSystem::write($dir.$insertId.".dat", $content);
			}
		}
	}

	/**
	 * @param string $rawBody
	 *
	 * @return string
	 */
	public function getMimeTypeOfRawBody(string $rawBody) : string
	{
		$temp_file = tempnam(sys_get_temp_dir(), 'request');
		file_put_contents($temp_file , $content);
		//$finfo = finfo_open(FILEINFO_MIME_TYPE);
		mime_content_type($temp_file);
		return mime_content_type($temp_file);//finfo_file($finfo, $temp_file);
	}

	/**
	 * @return void
	 */
	abstract public function beforeStartup();
	
	public function authenticate():void
	{

	}

	/**
	 * @param string $body
	 */
	public function sendXmlResponse(string $body, int $responseCode = 200) : void
	{
		$this->response->setCode($responseCode);
		$this->response->setHeader("Content-type", "text/xml");
		echo $body;
		exit;
	}

	/**
	 * @deprecated Please use ->sendJson() instead.
	 */
	public function sendJsonResponse(array $body = [], int $responseCode = 200) : void
	{
		$this->sendJson($body, $responseCode);
	}

	public function sendJson(array $body = [], int $responseCode = 200) : void {
		$response = [];
		if(isset($this->snippets) && !empty($this->snippets)){
			$response['snippets'] = $this->snippets;
			if(isset($body["snippets"]) && !empty($body["snippets"])){
				$response["snippets"] += $body["snippets"];
			}
		}
		if(!empty($payload = $this->getPayload())){
			$response['payload'] = $payload;
		}

		if(!empty($dataLayer = $this->context->getByType(Container::class)->getDataLayer())) {
			bdump($dataLayer, 'API dataLayer');
		}

		$application = $this->context->getByType(Application::class);
		$application->onShutdown($application);

		$this->router->sendJsonResponse($response + $body, $responseCode);
		//bdump($response + $body, "API response");
		exit;
	}
	
	public function redrawControl(string $name, array $parameters = []) : void
	{
		if(!Strings::contains($name, ".latte")){
			$name .= ".latte";
		}
		if(!($this->controller instanceof \CommerceController)) {
			throw new InvalidArgumentException('Controller not specified.');
		}
		$snippetId = $this->controller->getSnippetId($name);
		/*try {
			bdump($this->controller->render([], ["view" => $name] + $parameters, $parameters));
*/


		$this->snippets[$snippetId] = $this->controller->renderTemplate($name, $parameters);
//		} catch (\Throwable $e){
			/*$html = <<<EOT
<div class="my-account-box">
                <div class="my-account-box-header d-flex align-items-center">
                    <div>Základné informácie</div>
                    <div class="my-account-box-header-edit ml-auto tx-normal"><a href="javascript:;" onclick="javascript:popupOpen(&quot;/horti.sk/moje-konto?do=infoEdit&amp;presenter=MyAccount_controller&amp;id=1&quot;,500,null,100,1);" class="text-success"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Upraviť údaje</a></div>
                </div>
                <div class="my-account-box-content">
                    <div class="row mb-1">
                        <div class="col-lg-5">Meno:</div>
                        <div class="col-lg-7 tx-bold">HENVIL s.r.o</div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-lg-5">E-mail:</div>
                        <div class="col-lg-7 tx-bold">henvilsro@gmail.com</div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-lg-5">Telefón:</div>
                        <div class="col-lg-7 tx-bold"></div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-lg-5">Adresa:</div>
                        <div class="col-lg-7 tx-bold">Nám. Legionárov 13,  Prešov 08001, SLOVENSKO</div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-lg-5">Odber noviniek:</div>
                        <div class="col-lg-7 tx-bold">
                            <div id="snippet--myaccount-newsletter">
                                    <a href="javascript:newsletterChange();" class="text-success">aktívny</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
EOT;*/
/*			foreach (glob($this->controller->getLatteDir()."/*.latte") as $file){
				$f = new \SplFileInfo($file);
				//bdump($f->getFilename());
				try {
					$template = $this->controller->renderTemplate($f->getFilename(), $parameters);
					$match = Strings::match($template, '/<div id="'.$snippetId.'">([\S\s]*?)<\/div>/m');
					//bdump($match);
					if(isset($match[1])) {
						$this->snippets[$snippetId] = $match[1];
						}
				} catch (\Throwable $e){

				}
			}*/

			/*$dom = new \DOMDocument();
			$dom->loadHTML($html);
			$xpath = new \DOMXpath($dom);

			$div_list = $xpath->query('//div[@id="'.$snippetId.'"]');
			/** @var \DOMElement $item */
			/*foreach ($div_list as $item){
				$innerHTML= '';
				$children = $item->childNodes;
				foreach ($children as $child) {
					$innerHTML .= $child->ownerDocument->saveXML( $child );
				}
				echo $innerHTML; bdump($innerHTML);
			}*/

			/*$match = Strings::match($html, '/<div id="snippet--myaccount-newsletter">([\S\s]*?)<\/div>/m');
			bdump($match[1]);
*/
			//$this->snippets[$snippetId] = $match[1];
			//bdump($this->controller->getLatteDir());
			//bdump($snippetId);
		//}
	}

	public function redirect(string $url, int $code = Response::S302_FOUND): void
	{
		if($this->request->isAjax()){
			$this->payload->redirect = $url;
			$this->sendJson();
		}
		$this->response->redirect($url, $code);
		exit;
	}

	protected function throwError(string $message, int $code = 500) {
		$this->error = [$message, $code];
		Debugger::log($message, Logger::ERROR);
		$this->sendJson(["response" => ["message" => $message, "code" => $code]], $code);
	}

	protected function checkReferer() : bool {
		if(($referer = $this->request->getReferer()) !== null && Strings::contains($referer->getAbsoluteUrl(), $this->parameters->shop->url )){
			return true;
		}
		return false;
	}

	protected function getPayload() : array
	{
		$payload = [];
		foreach ($this->payload as $k => $v){
			$payload[$k] = $v;
		}
		return $payload;
	}

	public function sendResponse() : void {
		if(!empty($this->snippets) || !empty($this->getPayload())) {
			$this->sendJson();
		}
	}

}